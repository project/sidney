<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>
<body>

<div id="header">
	<div class="wrapper">
		<?php if (!empty($header_top_col1)): ?><div class="cols-1"><?php print $header_top_col1; ?></div><?php endif; ?>
		<div class="cols-2">
			<div class="branding">
		        <?php if (!empty($logo)): ?>
		          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
		            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		          </a>
		        <?php endif; ?>
	          <?php if (!empty($site_name)): ?>
	            <strong>
	              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
	            </strong>
	          <?php endif; ?>
	          <?php if (!empty($site_slogan)): ?>
	            <em><?php print $site_slogan; ?></em>
	          <?php endif; ?>		
			</div>
			<?php if (!empty($header_left_col2)): ?><?php print $header_left_col2; ?><?php endif; ?>			
		</div>
		<div class="cols-2">
			<?php print theme('links', $primary_links, array('class' => 'nav primary-links')); ?>
			<?php if (!empty($header_right_col2)): ?><?php print $header_right_col2; ?><?php endif; ?>			
		</div>	
		<?php if (!empty($header_bottom_col1)): ?><div class="cols-1"><?php print $header_bottom_col1; ?></div><?php endif; ?>
	</div>
</div>

<?php if ($is_front): ?>

<?php if (!empty($masthead)): ?>
<div id="masthead">
	<div class="wrapper">
		<?php print $masthead; ?>
	</div>
</div>
<?php endif; ?>		

<div id="frontpage-content">
	<div class="wrapper">
		<?php if (!empty($fpcontent_top_col1)): ?><div class="cols-1"><?php print $fpcontent_top_col1; ?></div><?php endif; ?>
		<?php if (!empty($fpcontent_left_col3)): ?><div class="cols-3"><?php print $fpcontent_left_col3; ?></div><?php endif; ?>	
		<?php if (!empty($fpcontent_center_col3)): ?><div class="cols-3"><?php print $fpcontent_center_col3; ?></div><?php endif; ?>				
		<?php if (!empty($fpcontent_right_col3)): ?><div class="cols-3"><?php print $fpcontent_right_col3; ?></div><?php endif; ?>							
		<?php if (!empty($fpcontent_bottom_col1)): ?><div class="cols-1"><?php print $fpcontent_bottom_col1; ?></div><?php endif; ?>
	</div>
</div>

<?php endif; ?>

<?php if (!$is_front): ?>
<div id="page-content">
	<div class="wrapper">
		<div class="cols-content">
			<div class="pagetitle">
				<?php if (!empty($breadcrumb)): ?><div id="breadcrumbs"><?php print $breadcrumb; ?></div><?php endif; ?>			
				<?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
			</div>
			<div class="pagemeta">
				<?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
				<?php if (!empty($messages)): print $messages; endif; ?>
				<?php if (!empty($help)): print $help; endif; ?>
			</div>
			<div class="content">
				<?php print $content; ?>
			</div>
		</div>
		<div class="cols-sidebar">
	      <?php if (!empty($sidebar)): ?>
	          <?php print $sidebar; ?>
	      <?php endif; ?>
		</div>		
	</div>
</div>
<?php endif; ?>


<div id="footer">
	<div class="wrapper">
		<?php if (!empty($footer_top_col1)): ?><div class="cols-1"><?php print $footer_top_col1; ?></div><?php endif; ?>
		<?php if (!empty($footer_message)): ?><div class="cols-1"><?php print $footer_message; ?></div><?php endif; ?>
		<div class="cols-2">
	        <?php if (!empty($secondary_links)): ?>
	            <?php print theme('links', $secondary_links, array('class' => 'nav secondary-links')); ?>
	        <?php endif; ?>
			<?php if (!empty($footer_left_col2)): ?>
				<?php print $footer_left_col2; ?>
			<?php endif; ?>
		</div>
		<?php if (!empty($footer_right_col2)): ?>
		<div class="cols-2">
	          <?php print $footer_right_col2; ?>
		</div>
		<?php endif; ?>
		<?php if (!empty($footer_bottom_col1)): ?><div class="cols-1"><?php print $footer_bottom_col1; ?></div><?php endif; ?>			
	</div>
</div>

<?php print $closure; ?>
</body>
</html>