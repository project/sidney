<div class="node">
	<?php print $picture ?>

	<?php if (!$page): ?>
		<h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
	<?php endif; ?>

	<div class="content">
		<?php print $content ?>
	</div>

	<?php print $links; ?>
</div>